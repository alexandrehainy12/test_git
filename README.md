# GIT
## Introduction a GIT
Git est un système de gestion de code source.

La première chose a faire pour utiliser GIT est d'initialiser un Repository (depuis VSCODE ou GIT Bash/GUI).

Pour initialiser le Repo, il suffit de taper la commande : 
```bash
cd /le/chemin/de/votre/dossier #Permet de se déplacer dans les dossiers suivants ou de reculer dans l'arborescence d'un ordinateur

git init #On initialise le Repo avec cet commande
```
Dans le terminal !

## Commit

### Qu'est-ce qu'un commit :

Un commit est un envoi de fichier permettant de crée une nouvelle entrée dans l'historique de votre Repo afin de le mettre a jour (et au cas ou le récupèrer si erreurs dans votre code). Ce fichier passe dans trois zones :

- La Zone de Travail
- La Zone d'Indexation
- La Zone de Fichiers Validés

### Comment faire un commit :

Il y a deux façons de faire un commit : par Ligne de Commande ou par VSCODE. Ces deux façons vont permettrent d'envoyez une nouvel entrée dans l'historique de vos fichiers et de votre Repo.

### En ligne de commande :

```bash
git status #On vérifie si il n'y a aucun commit en attente
git add <lefichierquevousvoulez> #Ici, on séléctionne le/les fichier(s) que l'on veut ajouter au commit
git commit -m "lemessagequevousvoulez" #C'est parti, le/les fichier(s) sont envoyés et indexés dans votre Repo avec votre joli message !
git log #Vous permet de voir vos commits (En gros, votre historique)
```
En cas de souci avec la ligne de commande, il suffit juste de vous inscrire sur Github et de renseigner avec ces lignes de commandes vos identifiants:
```bash

```

### Avec VSCODE :

Il suffit de cliquer sur le bouton Source Control afin de voir les fichiers qui ont été modifiés en local (Autrement dit, vos fichiers sont dans la Zone de Travail). Lorsque l'on choisi un fichier (Bouton + près du fichier au niveau de Changes), le fichier passe désormais en Zone d'Indexation. Il faut noter désormais un message juste au dessus pour avoir une meilleur précision lors des recherches dans l'historique de votre Repo et enfin cliquer sur l'icone V a côté de la flèche tournoyante en haut, ca y est, vos fichiers sont maintenant dans la Zone de Fichiers Validés. (Depuis les dernières versions, il faut cliquer sur la petite icone "Flèche du haut et Flèche du bas" afin d'envoyez le Commit sur Github)

## Memo Technique :

Repo = Repository = Dépot

VSCODE = Visual Studio Code